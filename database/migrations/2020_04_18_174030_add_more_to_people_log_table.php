<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreToPeopleLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people_logs', function (Blueprint $table) {
            $table->boolean('weakness')->default(false);
            $table->boolean('body_pain')->default(false);
            $table->boolean('sneezing')->default(false);
            $table->boolean('runny_nose')->default(false);
            $table->boolean('blocked_nose')->default(false);
            $table->boolean('throat_pain')->default(false);
            $table->boolean('loose_motion')->default(false);
            $table->boolean('watery_eye')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people_logs', function (Blueprint $table) {
            $table->dropColumn(['weakness', 'body_pain', 'sneezing', 'runny_nose', 'blocked_nose', 'throat_pain', 'loose_motion', 'watery_eye']);
        });
    }
}
