<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeopleLocationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->integer('people_id')->unsigned();
            $table->integer('vessel_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->boolean('screened');
            $table->text('note');
            $table->date('date');
            $table->boolean('positive')->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('people_id')->references('id')->on('people');
            $table->foreign('vessel_id')->references('id')->on('vessels');
            $table->foreign('parent_id')->references('id')->on('people_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people_locations');
    }
}
