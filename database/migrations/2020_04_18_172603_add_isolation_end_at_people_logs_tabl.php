<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsolationEndAtPeopleLogsTabl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people_logs', function (Blueprint $table) {
            $table->timestamp('isolation_end_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people_logs', function (Blueprint $table) {
            $table->dropColumn('isolation_end_at');
        });
    }
}
