<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeopleLogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned()->nullable();
            $table->integer('people_id')->unsigned()->nullable();
            $table->boolean('fever')->default(false);
            $table->boolean('shortness_of_breath')->default(false);
            $table->boolean('cough')->default(false);
            $table->enum('status',['normal','quarantine','home-isolation','hospitalized','critical','died']);
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('people_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people_logs');
    }
}
