<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVulnerablesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vulnerables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->integer('people_id')->unsigned();
            $table->integer('current_location_id')->unsigned();
            $table->boolean('hypertension')->default(false);
            $table->boolean('cardiovascular')->default(false);
            $table->boolean('chronic_respiratory_disease')->default(false);
            $table->boolean('cancer')->default(false);
            $table->boolean('old_age')->default(false);
            $table->string('remarks', 200)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('people_id')->references('id')->on('people');
            $table->foreign('current_location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vulnerables');
    }
}
