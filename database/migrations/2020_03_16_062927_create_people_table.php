<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeopleTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identifier', 255);
            $table->string('name', 255);
            $table->integer('location_id')->unsigned()->nullable();
            $table->string('contact');
            $table->string('current_address');
            $table->boolean('positive');
            $table->boolean('foreign')->default(false);
            $table->enum('status',['normal','quarantine','home-isolation','hospitalized','critical','died']);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people');
    }
}
