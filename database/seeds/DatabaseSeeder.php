<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VesselSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(UserSeeder::class);
//         $this->call(CountrySeeder::class);
        $this->call(CitizenSeeder::class);
        $this->call(LogSeeder::class);
        $this->call(VulnerablesSeed::class);
        $this->call(ResortSeeder::class);
        $this->call(AddressGenerator::class);

    }
}
