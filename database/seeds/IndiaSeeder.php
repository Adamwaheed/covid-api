<?php

use Illuminate\Database\Seeder;

class IndiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $strJsonFileContents = file_get_contents("https://raw.githubusercontent.com/bhanuc/indian-list/master/state-city.json");
        $json_file = json_decode($strJsonFileContents, true);
        $location = \App\Models\Location::whereName('India')->first();
        $location->parent_id =1;
        $location->save();
        foreach ($json_file as $key => $item) {
            $Asia = new \App\Models\Location();
            $Asia->name = $key;
            $Asia->code = $key;
            $Asia->prefix = $key;
            $Asia->type = 'state-city';
            $Asia->parent_id = $location->id;
            if ($Asia->save()) {
                foreach ($item as $subdivition) {
                    $sub = new \App\Models\Location();
                    $sub->name = $subdivition;
                    $sub->code = $subdivition;
                    $sub->prefix = $subdivition;
                    $sub->type = 'division';
                    $sub->parent_id = $Asia->id;
                    $sub->save();
                }
            }
        }

    }
}
