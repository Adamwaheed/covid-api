<?php

use Illuminate\Database\Seeder;

class AddressGenerator extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $peopleAddress = \App\Models\People::select('current_address')->groupBy('current_address')->get();
        foreach ($peopleAddress as $item) {
            $peopleLocation = \App\Models\People::where('current_address', $item->current_address)->first();
            $peoples = \App\Models\People::where('current_address', $item->current_address)->get();

            $Location = new \App\Models\Location();
            $Location->parent_id = $peopleLocation->location_id;
            $Location->name = $item->current_address;
            $Location->type = 'residence';
            $Location->prefix = $item->current_address;
            $Location->code = $item->current_address;
            if ($Location->save()) {
                foreach ($peoples as $people) {
                    $peopleAddress = new \App\Models\PeopleAddress();
                    $peopleAddress->people_id = $people->id;
                    $peopleAddress->location_id = $Location->id;
                    $peopleAddress->focal_point = false;
                    $peopleAddress->save();
                }
            }
        }
    }
}
