<?php

use Illuminate\Database\Seeder;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $citizen = \App\Models\People::all();
        foreach ($citizen as $item) {

            $this->logger($item);


        }
    }

    /**
     * @param $item
     */
    private function logger($item): void
    {
        $locatoncpunt = \App\Models\Location::count();
        for ($i = 0; $i < 3; $i++) {

            $locaton = new \App\Models\PeopleLocation();
            $locaton->location_id = rand(1, $locatoncpunt);
            $locaton->people_id = $item->id;
            $locaton->vessel_id = rand(1, 3);
            $locaton->screened = rand(1, 0);
            $locaton->note = 'nothing happned';
            $locaton->date = \Carbon\Carbon::now()->addDays($i);
            $locaton->positive = rand(1, 0);
            $locaton->save();

            $status = ['normal', 'quarantine', 'home-isolation', 'hospitalized', 'critical', 'died'];
            $people_log = new \App\Models\PeopleLog();
            $people_log->fever = rand(1, 0);
            $people_log->shortness_of_breath = rand(1, 0);
            $people_log->cough = rand(1, 0);
            $people_log->people_id = $item->id;
            $people_log->location_id = $locaton->location_id;
            $people_log->status = $status[rand(0, 5)];
            $people_log->save();
        }
    }
}
