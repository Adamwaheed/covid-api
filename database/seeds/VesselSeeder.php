<?php

use Illuminate\Database\Seeder;

class VesselSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'Kaavia','Bahaadharu','Speed','Vashafaru'
        ];
        foreach ($array as $item){
            $vessel = new \App\Models\Vessel();
            $vessel->name =$item;
            $vessel->created_by = 1;
            $vessel->registration_number = "A342342";
            $vessel->save();
        }

    }
}
