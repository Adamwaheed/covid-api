<?php

use Illuminate\Database\Seeder;

class ResortSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            ['name' => 'Kurumba Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Bandos Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Baros Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Sheraton Maldives Full Moon Resort & Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Velassaru Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Alimatha Aquatic Resort', 'atoll' => 'V', 'code' => 'K'],
            ['name' => 'Four Seasons Resort Maldives at Kuda HuR', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Kuramathi Maldives', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Banyan Tree Maldives Vabbinfaru', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Adaaran Club Rannalhi', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Kuredu Island Resort', 'atoll' => 'Lh', 'code' => 'LH'],
            ['name' => 'Meeru Island Resort', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Adaaran Prestige Vadoo', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Club Med Kanifinolhu', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Angsana Resort & Spa Maldives Ihuru', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Embudhu Village', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Oblu By Atmosphere at Helengeli', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Huvafenfushi Resort and Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Olhuveli Beach and Spa Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Coco Bodu Hithi', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'One & Only Reethi Rah, Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Paradise Island Resort and Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Club Med Finolhu Villas', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Adaaran Select Hudhuranfushi', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Centara Ras Fushi Resort & Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Thulhagiri Island Resort & Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Fun Island Resort & Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Rihiveli', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Anantara Resort and Spa Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Cinnamon Dhonveli Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Cocoa Island', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Fihaalhohi Island Resort', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Taj Coral Reef Resort and Spa', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'W Maldives', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Eriyadhu Island Resort', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Biyaadhoo Island Resort', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Dhiggiri Tourist Resort', 'atoll' => 'V', 'code' => 'V'],
            ['name' => 'Constance Halaveli Resort', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Jumeirah Vittaveli', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Makunudhoo Island', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Soneva Fushi Resort', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Nika Island Resort and Spa', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Summer Island Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Maayafushi Tourist Resort', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Taj Exotica Resort and Spa Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Veligandu Island Resort', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Holiday Inn Resort Kandooma Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Ellaidhoo Maldives By Cinnamon', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Naladhu', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Gangehi Island Resort', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Lux South Ari Atoll, Maldives', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Angaga Island Resort and Spa', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Diamonds Athuruga Beach & Water Villas', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Mirihi Island Resort', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Constance Moofushi Resort', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Diamonds Thudufushi Beach & Water Villas', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Amaya Kuda Rah Maldives', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Velidhoo Island Resort', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Conrad Maldives Rangali Island', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Centara Grand Island Resort & Spa Maldives', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Holiday Island Resort and Spa', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Vakarufalhi Island Resort', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Vilamendhoo Island Resort', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Lily Beach Resort', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Reethi Beach Resort', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Sun Aqua Vilu Reef Maldives', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Sun Island Resort and Spa', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Komandoo Maldive Island Resort', 'atoll' => 'Lh', 'code' => 'LH'],
            ['name' => 'Coco Palm Dhunikolhu', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Angsana Resort & Spa Maldives - Velavaru', 'atoll' => 'Dh', 'code' => 'DH'],
            ['name' => 'Kihaa Maldives', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Palm Beach Resort & Spa Maldives', 'atoll' => 'Lh', 'code' => 'LH'],
            ['name' => 'Kanuhura', 'atoll' => 'Lh', 'code' => 'LH'],
            ['name' => 'Filitheyo Island Resort', 'atoll' => 'Faafu', 'code' => 'F'],
            ['name' => 'Adaaran Select Meedhupparu', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Medhufushi Island Resort', 'atoll' => 'M', 'code' => 'M'],
            ['name' => 'Royal Island Resort and Spa', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Four Seasons Resort Maldives at Landaa GiRvaru', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Canareef Resort Maldives', 'atoll' => 'S', 'code' => 'S'],
            ['name' => 'JA Manafaru', 'atoll' => 'Haa Alifu', 'code' => 'HA'],
            ['name' => 'The Sun Siyam Iru Fushi Maldives', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Noku Maldives', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Shangri - Las Villingili Resort & Spa, Maldives', 'atoll' => 'S', 'code' => 'S'],
            ['name' => 'Park Hyatt Maldives, Hadahaa', 'atoll' => 'GA', 'code' => 'GA'],
            ['name' => 'Robinson Club Maldives', 'atoll' => 'GA', 'code' => 'GA'],
            ['name' => 'Anantara Kihavah Villas', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Six Senses Laamu', 'atoll' => 'Laamu', 'code' => 'L'],
            ['name' => 'Ayada Maldives', 'atoll' => 'GDh', 'code' => 'GDh'],
            ['name' => 'Dusit Thani Maldives', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'JW Marriott Maldives Resort & Spa', 'atoll' => 'Sh', 'code' => 'Sh'],
            ['name' => 'Niyama Maldives', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'The Residence Maldives', 'atoll' => 'GA', 'code' => 'GA'],
            ['name' => 'Velaa Private Island Maldives', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Cheval Blanc Randheli', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Atmosphere Kanifushi Maldives', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Maalifushi by Como', 'atoll' => 'Thaa', 'code' => 'Thaa'],
            ['name' => 'Kandolhu Island Maldives', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Outrigger Konotta Maldives Resort', 'atoll' => 'GDh', 'code' => 'GDh'],
            ['name' => 'aaaVeee Natures Paradise', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Amari Havodda Maldives', 'atoll' => 'GDh', 'code' => 'GDh'],
            ['name' => 'Furaveri Island Resort & Spa', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Finolhu B Atoll Maldives', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Ozen By Atmosphere At Maadhoo', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Malahini Kuda Bandos', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'The St. Regis Vommuli Resort, Maldives', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Dhigufaru Island Resort', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Soneva Jani', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Milaidhoo Island Maldives', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Hurawalhi Island Resort', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Four Seasons Private Island Maldives at Voavah', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Cocoon Maldives', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Kudafushi Resort & Spa', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Kandima Maldives', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Dhigali Maldives', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Grand Park Kodhipparu Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Fushifaru Maldives', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Robinson Club N', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Vakkaru Maldives', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Reethi Faru Resort', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Sirru Fen Fushi', 'atoll' => 'Sh', 'code' => 'Sh'],
            ['name' => 'Dreamland - The Unique Sea & Lake Resort/Spa', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Hondaafushi Island Resort', 'atoll' => 'Haa Dh', 'code' => 'Haa Dh'],
            ['name' => 'Oblu Select by Atmosphere at Sangeli', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'The Westin Maldives Miriandhoo Resort', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Joali Muravandhoo', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'M√∂venpick Resort Kuredhivaru Maldives', 'atoll' => 'N', 'code' => 'N'],
            ['name' => 'Kudadoo Maldives Private island', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Sun Aqua Iru Veli Maldives', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Raffles Maldives Meradhoo Resort', 'atoll' => 'GA', 'code' => 'GA'],
            ['name' => 'Carpe Diem Beach Resort and Spa Maldives', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Lux North Male Atoll', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'The Nautilus Maldives', 'atoll' => 'B', 'code' => 'B'],
            ['name' => 'Faarufushi Maldives', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'You & Me By Cocoon', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'The Residence Maldives At Dhigurah', 'atoll' => 'GA', 'code' => 'GA'],
            ['name' => 'Heritance Aarah', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Innahura Maldives Resort', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Baglioni Resort Maldives', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Waldorf Astoria Maldives Ithaafushi', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Riu Atoll and Riu Palace Maldivas', 'atoll' => 'Dh', 'code' => 'Dh'],
            ['name' => 'Saii Lagoon Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Palm Beach Resort & Spa Maldives', 'atoll' => 'Lh', 'code' => 'Lh'],
            ['name' => 'Drift Thelu Veliga Retreat', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Hard Rock Hotel Maldives', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Sandies Bathala', 'atoll' => 'AA', 'code' => 'AA'],
            ['name' => 'Emerald Maldives Resort & Spa Fasmendho', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Rahaa Resort', 'atoll' => 'Laamu', 'code' => 'L'],
            ['name' => 'Inter Continental Maldives Maamunagau', 'atoll' => 'R', 'code' => 'R'],
            ['name' => 'Pullman Maldives Maamutaa Resort', 'atoll' => 'GA', 'code' => 'GA'],
            ['name' => 'Cinnamon Velifushi Maldives', 'atoll' => 'V', 'code' => 'V'],
            ['name' => 'Varu Island Resort', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'South Palm Resort Maldives', 'atoll' => 'S', 'code' => 'S'],
            ['name' => 'Gili Lankanfushi', 'atoll' => 'K', 'code' => 'K'],
            ['name' => 'Cinnamon HakuR HuR Maldives', 'atoll' => 'M', 'code' => 'M'],
            ['name' => 'Radisson Blu Resort', 'atoll' => 'Adh', 'code' => 'Adh'],
            ['name' => 'Brennia Kottefaru', 'atoll' => 'R', 'code' => 'R'],
        ];

        foreach ($array as $item){
            $location = new \App\Models\Location();
            $location->name = $item['name'];
            $location->type = 'resorts';
            $location->prefix =  $item['code'];
            $location->code =  $item['code'];
            $parent = \App\Models\Location::whereType('atoll')->where('prefix',$item['code'])->first();
            if($parent){
                $location->parent_id =  $parent->id;
            }
            $location->save();

        }
    }
}
