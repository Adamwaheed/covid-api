<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'admin';
        $user->email = 'i@live.mv';
        $user->location_id = 79;
        $user->password = Hash::make('admin@1234');
        $user->type ='admin';
        $user->save();


        $userLGA = new \App\User();
        $userLGA->name = 'LGA';
        $userLGA->email = 'info@lga.gov.mv';
        $userLGA->password = Hash::make('admin@LGA');
        $userLGA->type ='lga';
        $userLGA->save();
    }
}
