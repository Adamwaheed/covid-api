<?php

use Illuminate\Database\Seeder;

class VulnerablesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $people = \App\Models\People::all();
        $count = \App\Models\Location::count();
        foreach ($people as $item) {
            $val = new \App\Models\Vulnerable();
            $val->location_id = rand(1, $count);
            $val->people_id = $item->id;
            $val->current_location_id =  rand(1, $count);
            $val->hypertension = rand(0, 1);
            $val->cardiovascular = rand(0, 1);
            $val->chronic_respiratory_disease = rand(0, 1);
            $val->cancer = rand(0, 1);
            $val->old_age = rand(0, 1);
            $val->remarks = "nothing";
            $val->save();
        }

    }
}
