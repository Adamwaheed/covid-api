<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PeopleLog;
use Faker\Generator as Faker;

$factory->define(PeopleLog::class, function (Faker $faker) {

    return [
        'location_id' => $faker->randomDigitNotNull,
        'people_id' => $faker->randomDigitNotNull,
        'fever' => $faker->word,
        'shortness_of_breath' => $faker->word,
        'cough' => $faker->word,
        'status' => $faker->word,
        'note' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
