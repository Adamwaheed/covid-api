<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Citizen;
use Faker\Generator as Faker;

$factory->define(Citizen::class, function (Faker $faker) {

    return [
        'NationalID' => $faker->word,
        'name' => $faker->word,
        'age' => $faker->randomDigitNotNull,
        'contactNumber' => $faker->word,
        'gender' => $faker->randomDigitNotNull,
        'consit' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
