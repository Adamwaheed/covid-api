<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vessel;
use Faker\Generator as Faker;

$factory->define(Vessel::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'created_by' => rand(1, 3),
        'registration_number' => "A" . rand(1000, 9999),
    ];
});
