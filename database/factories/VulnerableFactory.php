<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vulnerable;
use Faker\Generator as Faker;

$factory->define(Vulnerable::class, function (Faker $faker) {

    return [
        'location_id' => $faker->randomDigitNotNull,
        'people_id' => $faker->randomDigitNotNull,
        'current_location_id' => $faker->randomDigitNotNull,
        'hypertension' => $faker->word,
        'cardiovascular' => $faker->word,
        'chronic_respiratory_disease' => $faker->word,
        'cancer' => $faker->word,
        'remarks' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
