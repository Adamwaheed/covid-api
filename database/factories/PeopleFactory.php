<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\People;
use Faker\Generator as Faker;

$factory->define(People::class, function (Faker $faker) {

    return [
        'identifier' => $faker->word,
        'location_id' => $faker->randomDigitNotNull,
        'contact' => $faker->word,
        'current_address' => $faker->word,
        'positive' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
