<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Vessel::class, function (Faker $faker) {

    return [
        'name' => $faker->address,
    ];
});
