<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PeopleLocation;
use Faker\Generator as Faker;

$factory->define(PeopleLocation::class, function (Faker $faker) {

    return [
        'location_id' => $faker->randomDigitNotNull,
        'people_id' => $faker->randomDigitNotNull,
        'vessel_id' => $faker->randomDigitNotNull,
        'parent_id' => $faker->randomDigitNotNull,
        'screened' => $faker->word,
        'note' => $faker->text,
        'date' => $faker->word,
        'positive' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
