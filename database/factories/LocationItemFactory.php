<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LocationItem;
use Faker\Generator as Faker;

$factory->define(LocationItem::class, function (Faker $faker) {

    return [
        'location_id' => $faker->randomDigitNotNull,
        'item_id' => $faker->randomDigitNotNull,
        'qty' => $faker->randomDigitNotNull,
        'note' => $faker->text,
        'UOM' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
