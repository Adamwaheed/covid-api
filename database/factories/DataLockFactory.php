<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DataLock;
use Faker\Generator as Faker;

$factory->define(DataLock::class, function (Faker $faker) {

    return [
        'lock' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
