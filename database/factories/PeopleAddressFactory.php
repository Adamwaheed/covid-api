<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PeopleAddress;
use Faker\Generator as Faker;

$factory->define(PeopleAddress::class, function (Faker $faker) {

    return [
        'location_id' => $faker->randomDigitNotNull,
        'people_id' => $faker->randomDigitNotNull,
        'focal_point' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
