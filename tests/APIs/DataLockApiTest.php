<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DataLock;

class DataLockApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_data_lock()
    {
        $dataLock = factory(DataLock::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/data_locks', $dataLock
        );

        $this->assertApiResponse($dataLock);
    }

    /**
     * @test
     */
    public function test_read_data_lock()
    {
        $dataLock = factory(DataLock::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/data_locks/'.$dataLock->id
        );

        $this->assertApiResponse($dataLock->toArray());
    }

    /**
     * @test
     */
    public function test_update_data_lock()
    {
        $dataLock = factory(DataLock::class)->create();
        $editedDataLock = factory(DataLock::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/data_locks/'.$dataLock->id,
            $editedDataLock
        );

        $this->assertApiResponse($editedDataLock);
    }

    /**
     * @test
     */
    public function test_delete_data_lock()
    {
        $dataLock = factory(DataLock::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/data_locks/'.$dataLock->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/data_locks/'.$dataLock->id
        );

        $this->response->assertStatus(404);
    }
}
