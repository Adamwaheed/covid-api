<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PeopleAddress;

class PeopleAddressApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/people_addresses', $peopleAddress
        );

        $this->assertApiResponse($peopleAddress);
    }

    /**
     * @test
     */
    public function test_read_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/people_addresses/'.$peopleAddress->id
        );

        $this->assertApiResponse($peopleAddress->toArray());
    }

    /**
     * @test
     */
    public function test_update_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->create();
        $editedPeopleAddress = factory(PeopleAddress::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/people_addresses/'.$peopleAddress->id,
            $editedPeopleAddress
        );

        $this->assertApiResponse($editedPeopleAddress);
    }

    /**
     * @test
     */
    public function test_delete_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/people_addresses/'.$peopleAddress->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/people_addresses/'.$peopleAddress->id
        );

        $this->response->assertStatus(404);
    }
}
