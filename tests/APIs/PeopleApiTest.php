<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\People;

class PeopleApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_people()
    {
        $people = factory(People::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/people', $people
        );

        $this->assertApiResponse($people);
    }

    /**
     * @test
     */
    public function test_read_people()
    {
        $people = factory(People::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/people/'.$people->id
        );

        $this->assertApiResponse($people->toArray());
    }

    /**
     * @test
     */
    public function test_update_people()
    {
        $people = factory(People::class)->create();
        $editedPeople = factory(People::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/people/'.$people->id,
            $editedPeople
        );

        $this->assertApiResponse($editedPeople);
    }

    /**
     * @test
     */
    public function test_delete_people()
    {
        $people = factory(People::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/people/'.$people->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/people/'.$people->id
        );

        $this->response->assertStatus(404);
    }
}
