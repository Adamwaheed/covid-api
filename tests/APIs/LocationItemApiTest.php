<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LocationItem;

class LocationItemApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_location_item()
    {
        $locationItem = factory(LocationItem::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/location_items', $locationItem
        );

        $this->assertApiResponse($locationItem);
    }

    /**
     * @test
     */
    public function test_read_location_item()
    {
        $locationItem = factory(LocationItem::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/location_items/'.$locationItem->id
        );

        $this->assertApiResponse($locationItem->toArray());
    }

    /**
     * @test
     */
    public function test_update_location_item()
    {
        $locationItem = factory(LocationItem::class)->create();
        $editedLocationItem = factory(LocationItem::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/location_items/'.$locationItem->id,
            $editedLocationItem
        );

        $this->assertApiResponse($editedLocationItem);
    }

    /**
     * @test
     */
    public function test_delete_location_item()
    {
        $locationItem = factory(LocationItem::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/location_items/'.$locationItem->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/location_items/'.$locationItem->id
        );

        $this->response->assertStatus(404);
    }
}
