<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Vessel;

class VesselApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vessel()
    {
        $vessel = factory(Vessel::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/vessels', $vessel
        );

        $this->assertApiResponse($vessel);
    }

    /**
     * @test
     */
    public function test_read_vessel()
    {
        $vessel = factory(Vessel::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/vessels/'.$vessel->id
        );

        $this->assertApiResponse($vessel->toArray());
    }

    /**
     * @test
     */
    public function test_update_vessel()
    {
        $vessel = factory(Vessel::class)->create();
        $editedVessel = factory(Vessel::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/vessels/'.$vessel->id,
            $editedVessel
        );

        $this->assertApiResponse($editedVessel);
    }

    /**
     * @test
     */
    public function test_delete_vessel()
    {
        $vessel = factory(Vessel::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/vessels/'.$vessel->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/vessels/'.$vessel->id
        );

        $this->response->assertStatus(404);
    }
}
