<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Vulnerable;

class VulnerableApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/vulnerables', $vulnerable
        );

        $this->assertApiResponse($vulnerable);
    }

    /**
     * @test
     */
    public function test_read_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/vulnerables/'.$vulnerable->id
        );

        $this->assertApiResponse($vulnerable->toArray());
    }

    /**
     * @test
     */
    public function test_update_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->create();
        $editedVulnerable = factory(Vulnerable::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/vulnerables/'.$vulnerable->id,
            $editedVulnerable
        );

        $this->assertApiResponse($editedVulnerable);
    }

    /**
     * @test
     */
    public function test_delete_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/vulnerables/'.$vulnerable->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/vulnerables/'.$vulnerable->id
        );

        $this->response->assertStatus(404);
    }
}
