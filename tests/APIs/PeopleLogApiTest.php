<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PeopleLog;

class PeopleLogApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/people_logs', $peopleLog
        );

        $this->assertApiResponse($peopleLog);
    }

    /**
     * @test
     */
    public function test_read_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/people_logs/'.$peopleLog->id
        );

        $this->assertApiResponse($peopleLog->toArray());
    }

    /**
     * @test
     */
    public function test_update_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->create();
        $editedPeopleLog = factory(PeopleLog::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/people_logs/'.$peopleLog->id,
            $editedPeopleLog
        );

        $this->assertApiResponse($editedPeopleLog);
    }

    /**
     * @test
     */
    public function test_delete_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/people_logs/'.$peopleLog->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/people_logs/'.$peopleLog->id
        );

        $this->response->assertStatus(404);
    }
}
