<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PeopleLocation;

class PeopleLocationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/people_locations', $peopleLocation
        );

        $this->assertApiResponse($peopleLocation);
    }

    /**
     * @test
     */
    public function test_read_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/people_locations/'.$peopleLocation->id
        );

        $this->assertApiResponse($peopleLocation->toArray());
    }

    /**
     * @test
     */
    public function test_update_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->create();
        $editedPeopleLocation = factory(PeopleLocation::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/people_locations/'.$peopleLocation->id,
            $editedPeopleLocation
        );

        $this->assertApiResponse($editedPeopleLocation);
    }

    /**
     * @test
     */
    public function test_delete_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/people_locations/'.$peopleLocation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/people_locations/'.$peopleLocation->id
        );

        $this->response->assertStatus(404);
    }
}
