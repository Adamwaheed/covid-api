<?php namespace Tests\Repositories;

use App\Models\People;
use App\Repositories\PeopleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PeopleRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PeopleRepository
     */
    protected $peopleRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->peopleRepo = \App::make(PeopleRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_people()
    {
        $people = factory(People::class)->make()->toArray();

        $createdPeople = $this->peopleRepo->create($people);

        $createdPeople = $createdPeople->toArray();
        $this->assertArrayHasKey('id', $createdPeople);
        $this->assertNotNull($createdPeople['id'], 'Created People must have id specified');
        $this->assertNotNull(People::find($createdPeople['id']), 'People with given id must be in DB');
        $this->assertModelData($people, $createdPeople);
    }

    /**
     * @test read
     */
    public function test_read_people()
    {
        $people = factory(People::class)->create();

        $dbPeople = $this->peopleRepo->find($people->id);

        $dbPeople = $dbPeople->toArray();
        $this->assertModelData($people->toArray(), $dbPeople);
    }

    /**
     * @test update
     */
    public function test_update_people()
    {
        $people = factory(People::class)->create();
        $fakePeople = factory(People::class)->make()->toArray();

        $updatedPeople = $this->peopleRepo->update($fakePeople, $people->id);

        $this->assertModelData($fakePeople, $updatedPeople->toArray());
        $dbPeople = $this->peopleRepo->find($people->id);
        $this->assertModelData($fakePeople, $dbPeople->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_people()
    {
        $people = factory(People::class)->create();

        $resp = $this->peopleRepo->delete($people->id);

        $this->assertTrue($resp);
        $this->assertNull(People::find($people->id), 'People should not exist in DB');
    }
}
