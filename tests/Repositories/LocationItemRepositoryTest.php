<?php namespace Tests\Repositories;

use App\Models\LocationItem;
use App\Repositories\LocationItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LocationItemRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LocationItemRepository
     */
    protected $locationItemRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->locationItemRepo = \App::make(LocationItemRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_location_item()
    {
        $locationItem = factory(LocationItem::class)->make()->toArray();

        $createdLocationItem = $this->locationItemRepo->create($locationItem);

        $createdLocationItem = $createdLocationItem->toArray();
        $this->assertArrayHasKey('id', $createdLocationItem);
        $this->assertNotNull($createdLocationItem['id'], 'Created LocationItem must have id specified');
        $this->assertNotNull(LocationItem::find($createdLocationItem['id']), 'LocationItem with given id must be in DB');
        $this->assertModelData($locationItem, $createdLocationItem);
    }

    /**
     * @test read
     */
    public function test_read_location_item()
    {
        $locationItem = factory(LocationItem::class)->create();

        $dbLocationItem = $this->locationItemRepo->find($locationItem->id);

        $dbLocationItem = $dbLocationItem->toArray();
        $this->assertModelData($locationItem->toArray(), $dbLocationItem);
    }

    /**
     * @test update
     */
    public function test_update_location_item()
    {
        $locationItem = factory(LocationItem::class)->create();
        $fakeLocationItem = factory(LocationItem::class)->make()->toArray();

        $updatedLocationItem = $this->locationItemRepo->update($fakeLocationItem, $locationItem->id);

        $this->assertModelData($fakeLocationItem, $updatedLocationItem->toArray());
        $dbLocationItem = $this->locationItemRepo->find($locationItem->id);
        $this->assertModelData($fakeLocationItem, $dbLocationItem->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_location_item()
    {
        $locationItem = factory(LocationItem::class)->create();

        $resp = $this->locationItemRepo->delete($locationItem->id);

        $this->assertTrue($resp);
        $this->assertNull(LocationItem::find($locationItem->id), 'LocationItem should not exist in DB');
    }
}
