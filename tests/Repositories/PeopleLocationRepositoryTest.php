<?php namespace Tests\Repositories;

use App\Models\PeopleLocation;
use App\Repositories\PeopleLocationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PeopleLocationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PeopleLocationRepository
     */
    protected $peopleLocationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->peopleLocationRepo = \App::make(PeopleLocationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->make()->toArray();

        $createdPeopleLocation = $this->peopleLocationRepo->create($peopleLocation);

        $createdPeopleLocation = $createdPeopleLocation->toArray();
        $this->assertArrayHasKey('id', $createdPeopleLocation);
        $this->assertNotNull($createdPeopleLocation['id'], 'Created PeopleLocation must have id specified');
        $this->assertNotNull(PeopleLocation::find($createdPeopleLocation['id']), 'PeopleLocation with given id must be in DB');
        $this->assertModelData($peopleLocation, $createdPeopleLocation);
    }

    /**
     * @test read
     */
    public function test_read_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->create();

        $dbPeopleLocation = $this->peopleLocationRepo->find($peopleLocation->id);

        $dbPeopleLocation = $dbPeopleLocation->toArray();
        $this->assertModelData($peopleLocation->toArray(), $dbPeopleLocation);
    }

    /**
     * @test update
     */
    public function test_update_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->create();
        $fakePeopleLocation = factory(PeopleLocation::class)->make()->toArray();

        $updatedPeopleLocation = $this->peopleLocationRepo->update($fakePeopleLocation, $peopleLocation->id);

        $this->assertModelData($fakePeopleLocation, $updatedPeopleLocation->toArray());
        $dbPeopleLocation = $this->peopleLocationRepo->find($peopleLocation->id);
        $this->assertModelData($fakePeopleLocation, $dbPeopleLocation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_people_location()
    {
        $peopleLocation = factory(PeopleLocation::class)->create();

        $resp = $this->peopleLocationRepo->delete($peopleLocation->id);

        $this->assertTrue($resp);
        $this->assertNull(PeopleLocation::find($peopleLocation->id), 'PeopleLocation should not exist in DB');
    }
}
