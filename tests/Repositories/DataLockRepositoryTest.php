<?php namespace Tests\Repositories;

use App\Models\DataLock;
use App\Repositories\DataLockRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DataLockRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DataLockRepository
     */
    protected $dataLockRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->dataLockRepo = \App::make(DataLockRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_data_lock()
    {
        $dataLock = factory(DataLock::class)->make()->toArray();

        $createdDataLock = $this->dataLockRepo->create($dataLock);

        $createdDataLock = $createdDataLock->toArray();
        $this->assertArrayHasKey('id', $createdDataLock);
        $this->assertNotNull($createdDataLock['id'], 'Created DataLock must have id specified');
        $this->assertNotNull(DataLock::find($createdDataLock['id']), 'DataLock with given id must be in DB');
        $this->assertModelData($dataLock, $createdDataLock);
    }

    /**
     * @test read
     */
    public function test_read_data_lock()
    {
        $dataLock = factory(DataLock::class)->create();

        $dbDataLock = $this->dataLockRepo->find($dataLock->id);

        $dbDataLock = $dbDataLock->toArray();
        $this->assertModelData($dataLock->toArray(), $dbDataLock);
    }

    /**
     * @test update
     */
    public function test_update_data_lock()
    {
        $dataLock = factory(DataLock::class)->create();
        $fakeDataLock = factory(DataLock::class)->make()->toArray();

        $updatedDataLock = $this->dataLockRepo->update($fakeDataLock, $dataLock->id);

        $this->assertModelData($fakeDataLock, $updatedDataLock->toArray());
        $dbDataLock = $this->dataLockRepo->find($dataLock->id);
        $this->assertModelData($fakeDataLock, $dbDataLock->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_data_lock()
    {
        $dataLock = factory(DataLock::class)->create();

        $resp = $this->dataLockRepo->delete($dataLock->id);

        $this->assertTrue($resp);
        $this->assertNull(DataLock::find($dataLock->id), 'DataLock should not exist in DB');
    }
}
