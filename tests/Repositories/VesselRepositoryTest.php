<?php namespace Tests\Repositories;

use App\Models\Vessel;
use App\Repositories\VesselRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VesselRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VesselRepository
     */
    protected $vesselRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vesselRepo = \App::make(VesselRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vessel()
    {
        $vessel = factory(Vessel::class)->make()->toArray();

        $createdVessel = $this->vesselRepo->create($vessel);

        $createdVessel = $createdVessel->toArray();
        $this->assertArrayHasKey('id', $createdVessel);
        $this->assertNotNull($createdVessel['id'], 'Created Vessel must have id specified');
        $this->assertNotNull(Vessel::find($createdVessel['id']), 'Vessel with given id must be in DB');
        $this->assertModelData($vessel, $createdVessel);
    }

    /**
     * @test read
     */
    public function test_read_vessel()
    {
        $vessel = factory(Vessel::class)->create();

        $dbVessel = $this->vesselRepo->find($vessel->id);

        $dbVessel = $dbVessel->toArray();
        $this->assertModelData($vessel->toArray(), $dbVessel);
    }

    /**
     * @test update
     */
    public function test_update_vessel()
    {
        $vessel = factory(Vessel::class)->create();
        $fakeVessel = factory(Vessel::class)->make()->toArray();

        $updatedVessel = $this->vesselRepo->update($fakeVessel, $vessel->id);

        $this->assertModelData($fakeVessel, $updatedVessel->toArray());
        $dbVessel = $this->vesselRepo->find($vessel->id);
        $this->assertModelData($fakeVessel, $dbVessel->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vessel()
    {
        $vessel = factory(Vessel::class)->create();

        $resp = $this->vesselRepo->delete($vessel->id);

        $this->assertTrue($resp);
        $this->assertNull(Vessel::find($vessel->id), 'Vessel should not exist in DB');
    }
}
