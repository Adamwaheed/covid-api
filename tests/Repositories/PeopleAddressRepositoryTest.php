<?php namespace Tests\Repositories;

use App\Models\PeopleAddress;
use App\Repositories\PeopleAddressRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PeopleAddressRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PeopleAddressRepository
     */
    protected $peopleAddressRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->peopleAddressRepo = \App::make(PeopleAddressRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->make()->toArray();

        $createdPeopleAddress = $this->peopleAddressRepo->create($peopleAddress);

        $createdPeopleAddress = $createdPeopleAddress->toArray();
        $this->assertArrayHasKey('id', $createdPeopleAddress);
        $this->assertNotNull($createdPeopleAddress['id'], 'Created PeopleAddress must have id specified');
        $this->assertNotNull(PeopleAddress::find($createdPeopleAddress['id']), 'PeopleAddress with given id must be in DB');
        $this->assertModelData($peopleAddress, $createdPeopleAddress);
    }

    /**
     * @test read
     */
    public function test_read_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->create();

        $dbPeopleAddress = $this->peopleAddressRepo->find($peopleAddress->id);

        $dbPeopleAddress = $dbPeopleAddress->toArray();
        $this->assertModelData($peopleAddress->toArray(), $dbPeopleAddress);
    }

    /**
     * @test update
     */
    public function test_update_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->create();
        $fakePeopleAddress = factory(PeopleAddress::class)->make()->toArray();

        $updatedPeopleAddress = $this->peopleAddressRepo->update($fakePeopleAddress, $peopleAddress->id);

        $this->assertModelData($fakePeopleAddress, $updatedPeopleAddress->toArray());
        $dbPeopleAddress = $this->peopleAddressRepo->find($peopleAddress->id);
        $this->assertModelData($fakePeopleAddress, $dbPeopleAddress->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_people_address()
    {
        $peopleAddress = factory(PeopleAddress::class)->create();

        $resp = $this->peopleAddressRepo->delete($peopleAddress->id);

        $this->assertTrue($resp);
        $this->assertNull(PeopleAddress::find($peopleAddress->id), 'PeopleAddress should not exist in DB');
    }
}
