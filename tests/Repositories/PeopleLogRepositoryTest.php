<?php namespace Tests\Repositories;

use App\Models\PeopleLog;
use App\Repositories\PeopleLogRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PeopleLogRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PeopleLogRepository
     */
    protected $peopleLogRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->peopleLogRepo = \App::make(PeopleLogRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->make()->toArray();

        $createdPeopleLog = $this->peopleLogRepo->create($peopleLog);

        $createdPeopleLog = $createdPeopleLog->toArray();
        $this->assertArrayHasKey('id', $createdPeopleLog);
        $this->assertNotNull($createdPeopleLog['id'], 'Created PeopleLog must have id specified');
        $this->assertNotNull(PeopleLog::find($createdPeopleLog['id']), 'PeopleLog with given id must be in DB');
        $this->assertModelData($peopleLog, $createdPeopleLog);
    }

    /**
     * @test read
     */
    public function test_read_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->create();

        $dbPeopleLog = $this->peopleLogRepo->find($peopleLog->id);

        $dbPeopleLog = $dbPeopleLog->toArray();
        $this->assertModelData($peopleLog->toArray(), $dbPeopleLog);
    }

    /**
     * @test update
     */
    public function test_update_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->create();
        $fakePeopleLog = factory(PeopleLog::class)->make()->toArray();

        $updatedPeopleLog = $this->peopleLogRepo->update($fakePeopleLog, $peopleLog->id);

        $this->assertModelData($fakePeopleLog, $updatedPeopleLog->toArray());
        $dbPeopleLog = $this->peopleLogRepo->find($peopleLog->id);
        $this->assertModelData($fakePeopleLog, $dbPeopleLog->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_people_log()
    {
        $peopleLog = factory(PeopleLog::class)->create();

        $resp = $this->peopleLogRepo->delete($peopleLog->id);

        $this->assertTrue($resp);
        $this->assertNull(PeopleLog::find($peopleLog->id), 'PeopleLog should not exist in DB');
    }
}
