<?php namespace Tests\Repositories;

use App\Models\Vulnerable;
use App\Repositories\VulnerableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VulnerableRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VulnerableRepository
     */
    protected $vulnerableRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vulnerableRepo = \App::make(VulnerableRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->make()->toArray();

        $createdVulnerable = $this->vulnerableRepo->create($vulnerable);

        $createdVulnerable = $createdVulnerable->toArray();
        $this->assertArrayHasKey('id', $createdVulnerable);
        $this->assertNotNull($createdVulnerable['id'], 'Created Vulnerable must have id specified');
        $this->assertNotNull(Vulnerable::find($createdVulnerable['id']), 'Vulnerable with given id must be in DB');
        $this->assertModelData($vulnerable, $createdVulnerable);
    }

    /**
     * @test read
     */
    public function test_read_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->create();

        $dbVulnerable = $this->vulnerableRepo->find($vulnerable->id);

        $dbVulnerable = $dbVulnerable->toArray();
        $this->assertModelData($vulnerable->toArray(), $dbVulnerable);
    }

    /**
     * @test update
     */
    public function test_update_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->create();
        $fakeVulnerable = factory(Vulnerable::class)->make()->toArray();

        $updatedVulnerable = $this->vulnerableRepo->update($fakeVulnerable, $vulnerable->id);

        $this->assertModelData($fakeVulnerable, $updatedVulnerable->toArray());
        $dbVulnerable = $this->vulnerableRepo->find($vulnerable->id);
        $this->assertModelData($fakeVulnerable, $dbVulnerable->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vulnerable()
    {
        $vulnerable = factory(Vulnerable::class)->create();

        $resp = $this->vulnerableRepo->delete($vulnerable->id);

        $this->assertTrue($resp);
        $this->assertNull(Vulnerable::find($vulnerable->id), 'Vulnerable should not exist in DB');
    }
}
