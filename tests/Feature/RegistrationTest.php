<?php


namespace Tests\Feature;


use App\User;
use Tests\AttachesJWT;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use AttachesJWT;


    public function test_create_council()
    {
        $user = User::find(1);

        $this->loginAs($user);
        $data = [
            'name' => 'vaikaradhoo Council',
            'email' => 'vkd@livse.mv',
            'location_id' => '1',
            'password' => 'asdfasdf',
            'type' => 'council',
        ];
        $response = $this->json('POST', '/api/users', $data);


        $this->assertTrue(200);
    }

}
