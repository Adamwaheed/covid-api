<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 * @package App\Models
 * @version March 15, 2020, 4:19 pm UTC
 *
 * @property \App\Models\location parent
 * @property string name
 * @property integer parent_id
 * @property integer type
 * @property string prefix
 * @property string code
 * @property string longitude
 * @property string latitude
 */
class Location extends Model
{
    use SoftDeletes;

    public $table = 'locations';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'parent_id',
        'type',
        'prefix',
        'code',
        'longitude',
        'latitude',
        'status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'parent_id' => 'integer',
        'type' => 'string',
        'prefix' => 'string',
        'code' => 'string',
        'longitude' => 'string',
        'latitude' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required:string',
        'type' => 'required:string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parent()
    {
        return $this->belongsTo(\App\Models\location::class, 'parent_id', 'id');
    }

    public function island()
    {
        return $this->hasMany(\App\Models\location::class, 'parent_id', 'id');
    }

    public function citizens()
    {
        return $this->hasMany(PeopleAddress::class, 'location_id', 'id');
    }

    public function items()
    {
        return $this->hasMany(LocationItem::class, 'location_id', 'id');
    }


}
