<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vessel
 * @package App\Models
 * @version March 16, 2020, 6:37 am UTC
 *
 * @property string name
 */
class Vessel extends Model
{
    use SoftDeletes;

    public $table = 'vessels';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'registration_number',
        'created_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required:string'
    ];


}
