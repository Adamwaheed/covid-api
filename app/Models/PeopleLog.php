<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PeopleLog
 * @package App\Models
 * @version March 16, 2020, 6:43 am UTC
 *
 * @property \App\Models\location location
 * @property \App\Models\People people
 * @property integer location_id
 * @property integer people_id
 * @property boolean fever
 * @property boolean shortness_of_breath
 * @property boolean cough
 * @property string status
 * @property string note
 */
class PeopleLog extends Model
{
    use SoftDeletes;

    public $table = 'people_logs';

    public $appends = ['isolation_days', 'updated_today', 'last_update'];


    protected $dates = ['deleted_at'];


    public $fillable = [
        'location_id',
        'people_id',
        'fever',
        'shortness_of_breath',
        'cough',
        'status',
        'note',
        'tested_at',
        'isolation_end_at',
        'weakness',
        'body_pain',
        'sneezing',
        'runny_nose',
        'blocked_nose',
        'throat_pain',
        'loose_motion',
        'watery_eye',
        'test_result',
        'temperature',
    ];

    public function getIsolationDaysAttribute()
    {
        $isolation_end_at = Carbon::parse($this->isolation_end_at);
        return $isolation_end_at->diffInDays(today());
    }

    public function getUpdatedTodayAttribute()
    {
        return Carbon::parse($this->updated_at)->isToday();

    }

    public function getLastUpdateAttribute()
    {
        return Carbon::parse($this->updated_at)->diffForHumans();

    }


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'location_id' => 'integer',
        'people_id' => 'integer',
        'fever' => 'boolean',
        'shortness_of_breath' => 'boolean',
        'cough' => 'boolean',
        'weakness' => 'boolean',
        'body_pain' => 'boolean',
        'sneezing' => 'boolean',
        'runny_nose' => 'boolean',
        'blocked_nose' => 'boolean',
        'throat_pain' => 'boolean',
        'loose_motion' => 'boolean',
        'watery_eye' => 'boolean',
        'status' => 'string',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fever' => 'required:boolean',
        'shortness_of_breath' => 'required:boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function people()
    {
        return $this->belongsTo(\App\Models\People::class, 'people_id', 'id');
    }


    public static function boot()
    {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (PeopleLog $peopleLog) {
            $people = People::find($peopleLog->people_id);

            if ($people) {
                $people->status = $peopleLog->status;
                $people->save();
            }
        });


    }


}
