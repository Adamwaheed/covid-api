<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vulnerable
 * @package App\Models
 * @version March 21, 2020, 5:35 am UTC
 *
 * @property \App\Models\location location
 * @property \App\Models\People people
 * @property integer location_id
 * @property integer people_id
 * @property integer current_location_id
 * @property boolean hypertension
 * @property boolean cardiovascular
 * @property boolean chronic_respiratory_disease
 * @property boolean cancer
 * @property boolean old_age
 * @property string remarks
 */
class Vulnerable extends Model
{
    use SoftDeletes;

    public $table = 'vulnerables';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'location_id',
        'people_id',
        'current_location_id',
        'hypertension',
        'cardiovascular',
        'chronic_respiratory_disease',
        'cancer',
        'remarks',
        'old_age',
        'pregnant',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'location_id' => 'integer',
        'people_id' => 'integer',
        'current_location_id' => 'integer',
        'hypertension' => 'boolean',
        'cardiovascular' => 'boolean',
        'chronic_respiratory_disease' => 'boolean',
        'cancer' => 'boolean',
        'old_age' => 'boolean',
        'remarks' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function people()
    {
        return $this->belongsTo(\App\Models\People::class, 'people_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function current_location()
    {
        return $this->belongsTo(\App\Models\location::class, 'current_location_id', 'id');
    }
}
