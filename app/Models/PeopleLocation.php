<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class PeopleLocation
 * @package App\Models
 * @version March 16, 2020, 6:38 am UTC
 *
 * @property \App\Models\location location
 * @property \App\Models\People people
 * @property \App\Models\Vessels vessel
 * @property \App\Models\PeopleLocation parent
 * @property integer location_id
 * @property integer people_id
 * @property integer vessel_id
 * @property integer parent_id
 * @property boolean screened
 * @property string note
 * @property string date
 * @property boolean positive
 */
class PeopleLocation extends Model
{
    use SoftDeletes;

    public $table = 'people_locations';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'location_id',
        'people_id',
        'vessel_id',
        'parent_id',
        'screened',
        'note',
        'date',
        'positive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'location_id' => 'integer',
        'people_id' => 'integer',
        'vessel_id' => 'integer',
        'parent_id' => 'integer',
        'screened' => 'boolean',
        'note' => 'string',
        'date' => 'date',
        'positive' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'screened' => 'required',
        'note' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function people()
    {
        return $this->belongsTo(\App\Models\People::class, 'people_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vessel()
    {
        return $this->belongsTo(Vessel::class, 'vessel_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parent()
    {
        return $this->belongsTo(\App\Models\PeopleLocation::class, 'parent_id', 'id');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = \Carbon\Carbon::parse($value);
    }
}
