<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PeopleAddress
 * @package App\Models
 * @version April 19, 2020, 1:29 pm UTC
 *
 * @property \App\Models\location location
 * @property \App\Models\People people
 * @property integer location_id
 * @property integer people_id
 * @property boolean focal_point
 */
class PeopleAddress extends Model
{
    use SoftDeletes;

    public $table = 'people_addresses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'location_id',
        'people_id',
        'focal_point'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'location_id' => 'integer',
        'people_id' => 'integer',
        'focal_point' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'focal_point' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function people()
    {
        return $this->belongsTo(\App\Models\People::class, 'people_id', 'id');
    }
}
