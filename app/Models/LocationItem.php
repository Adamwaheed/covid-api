<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LocationItem
 * @package App\Models
 * @version April 19, 2020, 5:30 pm UTC
 *
 * @property \App\Models\location location
 * @property \App\Models\Item item
 * @property integer location_id
 * @property integer item_id
 * @property number qty
 * @property string note
 * @property string UOM
 */
class LocationItem extends Model
{
    use SoftDeletes;

    public $table = 'location_items';

    public $appends = ['run_out_on'];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'location_id',
        'item_id',
        'qty',
        'note',
        'UOM',
        'run_out_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'location_id' => 'integer',
        'item_id' => 'integer',
        'qty' => 'float',
        'note' => 'string',
        'UOM' => 'string'
    ];

    public function getRunOutOnAttribute(){
        $runOutAt = Carbon::parse($this->run_out_at);
        return $runOutAt->diffInDays(today());
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'qty' => 'required',
        'note' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\location::class, 'location_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class, 'item_id', 'id');
    }
}
