<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DataLock
 * @package App\Models
 * @version April 20, 2020, 11:26 am UTC
 *
 * @property \App\Models\User user
 * @property morph lock
 * @property integer user_id
 */
class DataLock extends Model
{
    use SoftDeletes;

    public $table = 'data_locks';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'lock',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }
}
