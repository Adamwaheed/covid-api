<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class People
 * @package App\Models
 * @version March 16, 2020, 6:29 am UTC
 *
 * @property \App\Models\location location
 * @property string identifier
 * @property integer location_id
 * @property string contact
 * @property string current_address
 * @property boolean positive
 * @property boolean status
 */
class People extends Model
{
    use SoftDeletes;

    public $table = 'people';


    protected $dates = ['deleted_at'];

    public $appends = ['age'];

    public $fillable = [
        'identifier',
        'location_id',
        'contact',
        'current_address',
        'positive',
        'status',
        'created_by',
        'dob',
        'name',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'identifier' => 'string',
        'location_id' => 'integer',
        'contact' => 'string',
        'current_address' => 'string',
        'positive' => 'boolean',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'identifier' => 'required:string',
        'contact' => 'required:string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\location::class, 'location_id', 'id');
    }

    public function people_locations()
    {
        return $this->hasMany(PeopleLocation::class, 'people_id', 'id')->orderBy('id','desc');
    }

    public function people_logs()
    {
        return $this->hasMany(PeopleLog::class, 'people_id', 'id')->orderBy('id','desc');
    }

    public function lastLog()
    {
        return $this->hasOne(PeopleLog::class, 'people_id', 'id')->orderBy('id','desc');
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->dob)->age;
    }

    public function vulnerable()
    {
        return $this->hasOne(Vulnerable::class);
    }
}
