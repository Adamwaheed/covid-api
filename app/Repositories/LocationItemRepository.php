<?php

namespace App\Repositories;

use App\Models\LocationItem;
use App\Repositories\BaseRepository;

/**
 * Class LocationItemRepository
 * @package App\Repositories
 * @version April 19, 2020, 5:30 pm UTC
*/

class LocationItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LocationItem::class;
    }
}
