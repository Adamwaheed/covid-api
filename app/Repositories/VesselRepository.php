<?php

namespace App\Repositories;

use App\Models\Vessel;
use App\Repositories\BaseRepository;

/**
 * Class VesselRepository
 * @package App\Repositories
 * @version March 16, 2020, 6:37 am UTC
*/

class VesselRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vessel::class;
    }
}
