<?php

namespace App\Repositories;

use App\Models\PeopleAddress;
use App\Repositories\BaseRepository;

/**
 * Class PeopleAddressRepository
 * @package App\Repositories
 * @version April 19, 2020, 1:29 pm UTC
*/

class PeopleAddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PeopleAddress::class;
    }
}
