<?php

namespace App\Repositories;

use App\Models\Vulnerable;
use App\Repositories\BaseRepository;

/**
 * Class VulnerableRepository
 * @package App\Repositories
 * @version March 21, 2020, 5:35 am UTC
*/

class VulnerableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vulnerable::class;
    }
}
