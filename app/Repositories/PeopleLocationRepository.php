<?php

namespace App\Repositories;

use App\Models\PeopleLocation;
use App\Repositories\BaseRepository;

/**
 * Class PeopleLocationRepository
 * @package App\Repositories
 * @version March 16, 2020, 6:38 am UTC
*/

class PeopleLocationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PeopleLocation::class;
    }
}
