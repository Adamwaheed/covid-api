<?php

namespace App\Repositories;

use App\Models\DataLock;
use App\Repositories\BaseRepository;

/**
 * Class DataLockRepository
 * @package App\Repositories
 * @version April 20, 2020, 11:26 am UTC
*/

class DataLockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DataLock::class;
    }
}
