<?php

namespace App\Repositories;

use App\Models\PeopleLog;
use App\Repositories\BaseRepository;

/**
 * Class PeopleLogRepository
 * @package App\Repositories
 * @version March 16, 2020, 6:43 am UTC
*/

class PeopleLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PeopleLog::class;
    }
}
