<?php

namespace App\Repositories;

use App\Models\People;
use App\Repositories\BaseRepository;

/**
 * Class PeopleRepository
 * @package App\Repositories
 * @version March 16, 2020, 6:29 am UTC
*/

class PeopleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identifier'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return People::class;
    }
}
