<?php


namespace App;


use App\Models\PeopleLog;

class Covid
{
    public function isolationEndAt($people_id, $status)
    {
        $peopleLogLast = PeopleLog::where('people_id', $people_id)->orderBy('id', 'desc')->first();

        $isolationPeriod = 0;

        if ($peopleLogLast) {

            if ($peopleLogLast->status != $status) {
                $isolationPeriod = $this->getIsolationPeriod($status);
            } else {
                return $peopleLogLast->isolation_end_at;
            }

        } else {
            $isolationPeriod = $this->getIsolationPeriod($status);

        }

        if ($isolationPeriod == 0) {
            return null;
        }

        return today()->addDays($isolationPeriod);
    }


    private function getIsolationPeriod($status)
    {
        if ($status == 'quarantine') {
            return env('quarantine_ISOLATION_PERIOD', 19);
        }

        if ($status == 'home-isolation') {
            return env('home-isolation_ISOLATION_PERIOD', 14);
        }


    }
}
