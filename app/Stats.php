<?php


namespace App;


use App\Models\Location;
use App\Models\PeopleLocation;
use App\Models\PeopleLog;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class Stats
{
    public $filterBy;
    public $fromDate;
    public $toDate;
    public $label = [];
    public $locations;
    public $locationId;
    public $locationType;

    public function __construct($fromDate = null, $toDate = null, $locationType = 'atolls', $locationId = null)
    {
        $this->fromDate = $fromDate ? $fromDate : Carbon::now()->addDays(-10);
        $this->locationType = $locationType;
        $this->toDate = $toDate ? $toDate : Carbon::now()->addDays(10);
        $this->locationId = $locationId;
        $this->setLabel();


    }

    public function createSeries($type)
    {
        $data = [];
        if ($this->locations) {
            foreach ($this->locations as $location) {
                if ($this->locationType == 'atolls') {
                    $islandIds = Location::whereType('island')->whereParentId($location->id)->get()->pluck('id');
                    $value = PeopleLog::whereIn('location_id', $islandIds)->whereBetween('created_at',[$this->fromDate,$this->toDate])->where($type, 1)->count();
                    array_push($data, $value);
                }

                if ($this->locationType == 'atoll') {
                    $value = PeopleLog::where('location_id', $location->id)->whereBetween('created_at',[$this->fromDate,$this->toDate])->where($type, 1)->count();
                    array_push($data, $value);
                }
            }
        }

        if ($this->locationType == 'island') {
            foreach ($this->label as $item) {
                $value = PeopleLog::where('location_id', $this->locationId)
                    ->whereDate('created_at', $item)
                    ->where($type, 1)->count();
                array_push($data, $value);
            }

        }

        return $data;

    }

    public function setLabel(): void
    {
        if ($this->locationType == 'atolls') {
            $this->locations = Location::where('type', 'atoll')->get();
            $this->label = explode(',', $this->locations->implode('name', ', '));
        }

        if ($this->locationType == 'atoll' && !is_null($this->locationId)) {
            $this->locations = Location::where('type', 'island')->whereParentId($this->locationId)->get();
            $this->label = explode(',', $this->locations->implode('name', ', '));
        }

        if ($this->locationType == 'island' && !is_null($this->locationId)) {
            $period = CarbonPeriod::create($this->fromDate->toDate(), $this->toDate->toDate());

            foreach ($period as $date) {
                array_push($this->label, $date->format('Y-m-d'));
            }


        }


    }


    public function travelSeries()
    {
        $data = [];
        if ($this->locations) {
            foreach ($this->locations as $location) {
                if ($this->locationType == 'atolls') {
                    $islandIds = Location::whereType('island')->whereParentId($location->id)->get()->pluck('id');
                    $value = PeopleLocation::whereIn('location_id', $islandIds)->whereBetween('created_at',[$this->fromDate,$this->toDate])->count();
                    array_push($data, $value);
                }

                if ($this->locationType == 'atoll') {
                    $value = PeopleLocation::where('location_id', $location->id)->whereBetween('created_at',[$this->fromDate,$this->toDate])->count();
                    array_push($data, $value);
                }
            }
        }

        if ($this->locationType == 'island') {
            foreach ($this->label as $item) {
                $value = PeopleLocation::where('location_id', $this->locationId)
                    ->whereDate('created_at', $item)->count();
                array_push($data, $value);
            }

        }

        return $data;

    }


    public function symptomsStatus($status)
    {
        $data = [];
        if ($this->locations) {
            foreach ($this->locations as $location) {
                if ($this->locationType == 'atolls') {
                    $islandIds = Location::whereType('island')->whereParentId($location->id)->get()->pluck('id');
                    $value = PeopleLog::whereIn('location_id', $islandIds)->whereBetween('created_at',[$this->fromDate,$this->toDate])
                        ->where('status',$status)->count();
                    array_push($data, $value);
                }

                if ($this->locationType == 'atoll') {
                    $value = PeopleLog::where('location_id', $location->id)->whereBetween('created_at',[$this->fromDate,$this->toDate])
                        ->where('status',$status)
                        ->count();
                    array_push($data, $value);
                }
            }
        }

        if ($this->locationType == 'island') {
            foreach ($this->label as $item) {
                $value = PeopleLog::where('location_id', $this->locationId)
                    ->whereDate('created_at', $item)
                    ->where('status',$status)->count();
                array_push($data, $value);
            }

        }

        return $data;

    }
}
