<?php


namespace App;


use App\Models\Location;
use App\Models\PeopleLocation;

class Statistics
{
    public $series;
    public $locationType;
    public $locationId;

    public function __construct($locationType, $locationId)
    {
        $this->setSeries($locationType);
        $this->locationId = $locationId;
    }

    public function setSeries($locationType)
    {
        if ($locationType == 'atolls') {
            $this->series = Location::whereType('atoll')->get();
        }

        if ($locationType == 'atoll') {
            $this->series = Location::whereType('island')->whereParentId($this->locationId)->get();
        }
    }

}
