<?php


namespace App;


use App\Models\People;

class Kanban
{
    public function listTest()
    {
        return $people = People::whereHas('lastLog', function ($log) {
            $log->whereNotNull('tested_at')->whereNull('test_result');
        })->with('lastLog')->get();
    }

    public function listPositive()
    {
        return $people = People::whereHas('lastLog', function ($log) {
            $log->whereNotNull('tested_at')->where('test_result', 'positive')->where('status', '!=', 'normal');
        })->with('lastLog')->get();
    }

    public function listNegative()
    {
        return $people = People::whereHas('lastLog', function ($log) {
            $log->whereNotNull('tested_at')->where('test_result', 'negative');
        })->with('lastLog')->get();
    }

    public function cured()
    {
        return $people = People::whereHas('lastLog', function ($log) {
            $log->whereNotNull('tested_at')->where('test_result', 'positive')->whereStatus('normal');
        })->with('lastLog')->get();
    }

    public function toString()
    {
        return [
            'listTest' => $this->listTest(),
            'listCured' => $this->cured(),
            'listNegative' => $this->listNegative(),
            'listPositive' => $this->listPositive(),
        ];
    }
}
