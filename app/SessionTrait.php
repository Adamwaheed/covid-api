<?php


namespace App;


trait SessionTrait
{

    public function authUserLocationId()
    {
        return auth()->user()->location_id;

    }

    public function isAdmin()
    {
        return auth()->user()->type == 'admin';
    }

    public function isLga()
    {
        return auth()->user()->type == 'lga';
    }

    public function isCouncil()
    {
        return auth()->user()->type == 'council';
    }

    public function isNotCouncil()
    {
        return auth()->user()->type != 'council';
    }
}
