<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLocationAPIRequest;
use App\Http\Requests\API\UpdateLocationAPIRequest;
use App\Models\Location;
use App\Repositories\LocationRepository;
use App\SessionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Response;

/**
 * Class LocationController
 * @package App\Http\Controllers\API
 */
class LocationAPIController extends AppBaseController
{
    /** @var  LocationRepository */
    private $locationRepository;
    use SessionTrait;

    public function __construct(LocationRepository $locationRepo)
    {
        $this->locationRepository = $locationRepo;
    }

    /**
     * Display a listing of the Location.
     * GET|HEAD /locations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {


        $locations = Location::take(18);
        if ($request->search && !is_null($request->search)) {
            $locations->where('name', 'like', '%' . $request->search . '%');
        }
        if ($request->type && !is_null($request->type)) {
            $locations->whereType($request->type);
        }


        return $this->sendResponse($locations->get(), 'Locations retrieved successfully');
    }

    /**
     * Store a newly created Location in storage.
     * POST /locations
     *
     * @param CreateLocationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLocationAPIRequest $request)
    {
        $input = $request->all();

        if ($this->isCouncil()) {

            $input['parent_id'] = $this->authUserLocationId();

        }

        $location = $this->locationRepository->create($input);

        return $this->sendResponse($location->toArray(), 'Location saved successfully');
    }

    /**
     * Display the specified Location.
     * GET|HEAD /locations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Location $location */
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            return $this->sendError('Location not found');
        }

        return $this->sendResponse($location->toArray(), 'Location retrieved successfully');
    }

    /**
     * Update the specified Location in storage.
     * PUT/PATCH /locations/{id}
     *
     * @param int $id
     * @param UpdateLocationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Location $location */
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            return $this->sendError('Location not found');
        }

        $location = $this->locationRepository->update($input, $id);

        return $this->sendResponse($location->toArray(), 'Location updated successfully');
    }

    /**
     * Remove the specified Location from storage.
     * DELETE /locations/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var Location $location */
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            return $this->sendError('Location not found');
        }

        $location->delete();

        return $this->sendSuccess('Location deleted successfully');
    }


    public function atolls()
    {
        $locations = Location::whereType('atoll')->with(['island'])->get();

        return $this->sendResponse($locations, 'Location deleted successfully');
    }


    public function residence(Request $request)
    {
        $locations = Location::whereType('residence');

        if ($request->search) {
            $locations->where('name', 'like', '%' . $request->search . '%');
        }

        return $this->sendResponse($locations->paginate(), 'Location deleted successfully');
    }

    public function residenceStock()
    {
        $dates = [today(), today()->addDays(4)];

        $data = Location::whereType('residence')->whereHas('items', function ($items) use ($dates) {
            $items->whereBetween('run_out_at', $dates);
        })->with(['items.item','citizens'])->get();

        return $this->sendResponse($data->toArray(), 'Stock listed successfully');
    }

    public function updateResidence()
    {
       //pick free  data


    }


}
