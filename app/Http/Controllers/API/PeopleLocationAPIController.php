<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePeopleLocationAPIRequest;
use App\Http\Requests\API\UpdatePeopleLocationAPIRequest;
use App\Models\PeopleLocation;
use App\Models\Vessel;
use App\Repositories\PeopleLocationRepository;
use App\Repositories\PeopleLogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PeopleLocationController
 * @package App\Http\Controllers\API
 */
class PeopleLocationAPIController extends AppBaseController
{
    /** @var  PeopleLocationRepository */
    private $peopleLocationRepository;
    private $logRepository;

    public function __construct(PeopleLocationRepository $peopleLocationRepo, PeopleLogRepository $logRepository)
    {
        $this->peopleLocationRepository = $peopleLocationRepo;
        $this->logRepository = $logRepository;
    }

    /**
     * Display a listing of the PeopleLocation.
     * GET|HEAD /peopleLocations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $peopleLocations = $this->peopleLocationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($peopleLocations->toArray(), 'People Locations retrieved successfully');
    }

    /**
     * Store a newly created PeopleLocation in storage.
     * POST /peopleLocations
     *
     * @param CreatePeopleLocationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePeopleLocationAPIRequest $request)
    {
        $input = $request->only(['vessel_id', 'location_id', 'people_id', 'screened', 'note', 'date']);

        if (is_null($request->note) || $request->note) {
            $input['note'] = 'NILL';
        }

        if ($request->vessel_name) {

            $vessel = Vessel::firstOrCreate(['name' => $request->vessel_name]);

            $input['vessel_id'] = $vessel->id;
        }

        $peopleLocation = $this->peopleLocationRepository->create($input);
        $peopleLocation->load(['vessel', 'location']);

        return $this->sendResponse($peopleLocation->toArray(), 'People Location saved successfully');
    }

    /**
     * Display the specified PeopleLocation.
     * GET|HEAD /peopleLocations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PeopleLocation $peopleLocation */
        $peopleLocation = $this->peopleLocationRepository->find($id);

        if (empty($peopleLocation)) {
            return $this->sendError('People Location not found');
        }

        return $this->sendResponse($peopleLocation->toArray(), 'People Location retrieved successfully');
    }

    /**
     * Update the specified PeopleLocation in storage.
     * PUT/PATCH /peopleLocations/{id}
     *
     * @param int $id
     * @param UpdatePeopleLocationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeopleLocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var PeopleLocation $peopleLocation */
        $peopleLocation = $this->peopleLocationRepository->find($id);

        if (empty($peopleLocation)) {
            return $this->sendError('People Location not found');
        }

        $peopleLocation = $this->peopleLocationRepository->update($input, $id);

        return $this->sendResponse($peopleLocation->toArray(), 'PeopleLocation updated successfully');
    }

    /**
     * Remove the specified PeopleLocation from storage.
     * DELETE /peopleLocations/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var PeopleLocation $peopleLocation */
        $peopleLocation = $this->peopleLocationRepository->find($id);

        if (empty($peopleLocation)) {
            return $this->sendError('People Location not found');
        }

        $peopleLocation->delete();

        return $this->sendSuccess('People Location deleted successfully');
    }
}
