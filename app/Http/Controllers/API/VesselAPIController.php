<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVesselAPIRequest;
use App\Http\Requests\API\UpdateVesselAPIRequest;
use App\Models\Vessel;
use App\Repositories\VesselRepository;
use App\SessionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class VesselController
 * @package App\Http\Controllers\API
 */
class VesselAPIController extends AppBaseController
{
    /** @var  VesselRepository */
    private $vesselRepository;
    use SessionTrait;

    public function __construct(VesselRepository $vesselRepo)
    {
        $this->vesselRepository = $vesselRepo;
    }

    /**
     * Display a listing of the Vessel.
     * GET|HEAD /vessels
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $vessels = Vessel::take(18);
        if ($request->search && !is_null($request->search)) {
            $vessels->where('name', 'like', '%' . $request->search . '%');
        }
        return $this->sendResponse($vessels->get(), 'Locations retrieved successfully');
    }

    /**
     * Store a newly created Vessel in storage.
     * POST /vessels
     *
     * @param CreateVesselAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVesselAPIRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = auth()->user()->id;

        $vessel = $this->vesselRepository->create($input);

        return $this->sendResponse($vessel->toArray(), 'Vessel saved successfully');
    }

    /**
     * Display the specified Vessel.
     * GET|HEAD /vessels/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Vessel $vessel */
        $vessel = $this->vesselRepository->find($id);

        if (empty($vessel)) {
            return $this->sendError('Vessel not found');
        }

        return $this->sendResponse($vessel->toArray(), 'Vessel retrieved successfully');
    }

    /**
     * Update the specified Vessel in storage.
     * PUT/PATCH /vessels/{id}
     *
     * @param int $id
     * @param UpdateVesselAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVesselAPIRequest $request)
    {
        $input = $request->all();

        /** @var Vessel $vessel */
        $vessel = $this->vesselRepository->find($id);

        if (empty($vessel)) {
            return $this->sendError('Vessel not found');
        }

        $vessel = $this->vesselRepository->update($input, $id);

        return $this->sendResponse($vessel->toArray(), 'Vessel updated successfully');
    }

    /**
     * Remove the specified Vessel from storage.
     * DELETE /vessels/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var Vessel $vessel */
        $vessel = $this->vesselRepository->find($id);

        if (empty($vessel)) {
            return $this->sendError('Vessel not found');
        }

        $vessel->delete();

        return $this->sendSuccess('Vessel deleted successfully');
    }
}
