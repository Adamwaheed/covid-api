<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDataLockAPIRequest;
use App\Http\Requests\API\UpdateDataLockAPIRequest;
use App\Models\DataLock;
use App\Repositories\DataLockRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DataLockController
 * @package App\Http\Controllers\API
 */

class DataLockAPIController extends AppBaseController
{
    /** @var  DataLockRepository */
    private $dataLockRepository;

    public function __construct(DataLockRepository $dataLockRepo)
    {
        $this->dataLockRepository = $dataLockRepo;
    }

    /**
     * Display a listing of the DataLock.
     * GET|HEAD /dataLocks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $dataLocks = $this->dataLockRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dataLocks->toArray(), 'Data Locks retrieved successfully');
    }

    /**
     * Store a newly created DataLock in storage.
     * POST /dataLocks
     *
     * @param CreateDataLockAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDataLockAPIRequest $request)
    {
        $input = $request->all();

        $dataLock = $this->dataLockRepository->create($input);

        return $this->sendResponse($dataLock->toArray(), 'Data Lock saved successfully');
    }

    /**
     * Display the specified DataLock.
     * GET|HEAD /dataLocks/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DataLock $dataLock */
        $dataLock = $this->dataLockRepository->find($id);

        if (empty($dataLock)) {
            return $this->sendError('Data Lock not found');
        }

        return $this->sendResponse($dataLock->toArray(), 'Data Lock retrieved successfully');
    }

    /**
     * Update the specified DataLock in storage.
     * PUT/PATCH /dataLocks/{id}
     *
     * @param int $id
     * @param UpdateDataLockAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDataLockAPIRequest $request)
    {
        $input = $request->all();

        /** @var DataLock $dataLock */
        $dataLock = $this->dataLockRepository->find($id);

        if (empty($dataLock)) {
            return $this->sendError('Data Lock not found');
        }

        $dataLock = $this->dataLockRepository->update($input, $id);

        return $this->sendResponse($dataLock->toArray(), 'DataLock updated successfully');
    }

    /**
     * Remove the specified DataLock from storage.
     * DELETE /dataLocks/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DataLock $dataLock */
        $dataLock = $this->dataLockRepository->find($id);

        if (empty($dataLock)) {
            return $this->sendError('Data Lock not found');
        }

        $dataLock->delete();

        return $this->sendSuccess('Data Lock deleted successfully');
    }
}
