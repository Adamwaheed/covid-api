<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLocationItemAPIRequest;
use App\Http\Requests\API\UpdateLocationItemAPIRequest;
use App\Models\LocationItem;
use App\Repositories\LocationItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LocationItemController
 * @package App\Http\Controllers\API
 */
class LocationItemAPIController extends AppBaseController
{
    /** @var  LocationItemRepository */
    private $locationItemRepository;

    public function __construct(LocationItemRepository $locationItemRepo)
    {
        $this->locationItemRepository = $locationItemRepo;
    }

    /**
     * Display a listing of the LocationItem.
     * GET|HEAD /locationItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $locationItems = $this->locationItemRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($locationItems->toArray(), 'Location Items retrieved successfully');
    }

    /**
     * Store a newly created LocationItem in storage.
     * POST /locationItems
     *
     * @param CreateLocationItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLocationItemAPIRequest $request)
    {
        $input = $request->all();

        $input['run_out_at'] = today()->addDays($request->run_out_on);

        $locationItem = $this->locationItemRepository->create($input);

        return $this->sendResponse($locationItem->toArray(), 'Location Item saved successfully');
    }

    /**
     * Display the specified LocationItem.
     * GET|HEAD /locationItems/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LocationItem $locationItem */
        $locationItem = $this->locationItemRepository->find($id);

        if (empty($locationItem)) {
            return $this->sendError('Location Item not found');
        }

        return $this->sendResponse($locationItem->toArray(), 'Location Item retrieved successfully');
    }

    /**
     * Update the specified LocationItem in storage.
     * PUT/PATCH /locationItems/{id}
     *
     * @param int $id
     * @param UpdateLocationItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocationItemAPIRequest $request)
    {
        $input = $request->all();

        $input['run_out_at'] = today()->addDays($request->run_out_on);
        /** @var LocationItem $locationItem */
        $locationItem = $this->locationItemRepository->find($id);

        if (empty($locationItem)) {
            return $this->sendError('Location Item not found');
        }

        $locationItem = $this->locationItemRepository->update($input, $id);

        return $this->sendResponse($locationItem->toArray(), 'LocationItem updated successfully');
    }

    /**
     * Remove the specified LocationItem from storage.
     * DELETE /locationItems/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var LocationItem $locationItem */
        $locationItem = $this->locationItemRepository->find($id);

        if (empty($locationItem)) {
            return $this->sendError('Location Item not found');
        }

        $locationItem->delete();

        return $this->sendSuccess('Location Item deleted successfully');
    }
}
