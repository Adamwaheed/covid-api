<?php

namespace App\Http\Controllers\API;

use App\Covid;
use App\Http\Requests\API\CreatePeopleLogAPIRequest;
use App\Http\Requests\API\UpdatePeopleLogAPIRequest;
use App\Models\PeopleLog;
use App\Repositories\PeopleLogRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PeopleLogController
 * @package App\Http\Controllers\API
 */
class PeopleLogAPIController extends AppBaseController
{
    /** @var  PeopleLogRepository */
    private $peopleLogRepository;
    public $covid;


    public function __construct(PeopleLogRepository $peopleLogRepo, Covid $covid)
    {
        $this->peopleLogRepository = $peopleLogRepo;
        $this->covid = $covid;

    }

    /**
     * Display a listing of the PeopleLog.
     * GET|HEAD /peopleLogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $peopleLogs = $this->peopleLogRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($peopleLogs->toArray(), 'People Logs retrieved successfully');
    }

    /**
     * Store a newly created PeopleLog in storage.
     * POST /peopleLogs
     *
     * @param CreatePeopleLogAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePeopleLogAPIRequest $request)
    {
        $input = $request->all();


        $input['tested_at'] = Carbon::parse($request->get('tested_at'));

        $input['isolation_end_at'] = $this->covid->isolationEndAt($request->people_id, $request->status);

        if ($request->extend_isolation) {
            $input['isolation_end_at'] = today()->addDays($request->extend_isolation);
        }


        $peopleLog = $this->peopleLogRepository->create($input);

        return $this->sendResponse($peopleLog->toArray(), 'People Log saved successfully');
    }

    /**
     * Display the specified PeopleLog.
     * GET|HEAD /peopleLogs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PeopleLog $peopleLog */
        $peopleLog = $this->peopleLogRepository->find($id);

        if (empty($peopleLog)) {
            return $this->sendError('People Log not found');
        }

        return $this->sendResponse($peopleLog->toArray(), 'People Log retrieved successfully');
    }

    /**
     * Update the specified PeopleLog in storage.
     * PUT/PATCH /peopleLogs/{id}
     *
     * @param int $id
     * @param UpdatePeopleLogAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeopleLogAPIRequest $request)
    {
        $input = $request->all();

        /** @var PeopleLog $peopleLog */
        $peopleLog = $this->peopleLogRepository->find($id);

        if (empty($peopleLog)) {
            return $this->sendError('People Log not found');
        }

        $peopleLog = $this->peopleLogRepository->update($input, $id);

        return $this->sendResponse($peopleLog->toArray(), 'PeopleLog updated successfully');
    }

    /**
     * Remove the specified PeopleLog from storage.
     * DELETE /peopleLogs/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var PeopleLog $peopleLog */
        $peopleLog = $this->peopleLogRepository->find($id);

        if (empty($peopleLog)) {
            return $this->sendError('People Log not found');
        }

        $peopleLog->delete();

        return $this->sendSuccess('People Log deleted successfully');
    }
}
