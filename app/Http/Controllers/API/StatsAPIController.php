<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\People;
use App\Models\PeopleLocation;
use App\Models\PeopleLog;
use App\Models\Vulnerable;
use App\Stats;
use Illuminate\Http\Request;

class StatsAPIController extends AppBaseController
{
    public function overView(Request $request)
    {
        $data = People::select(\DB::raw('count(*) as count'), 'status');


        if ($request->location_type == 'atoll') {

            $islandIds = Location::whereType('island')->whereParentId($request->location_id)->get()->pluck('id');
            $data = $data->whereIn('location_id', $islandIds);

        }


        if ($request->location_type == 'island') {

            $data = $data->where('location_id', $request->location_id);

        }

        $data->groupBy('status');

        return $this->sendResponse($data->get(), 'overview of people status');
    }

    public function dashboard(Request $request)
    {
        $fever = [];
        $sob = [];
        $cough = [];
        $vulnerable = [];

        if ($request->location_type == 'atolls') {
            $series = Location::whereType('atoll')->get();
        }

        if ($request->location_type == 'atoll' && $request->location_id) {
            $series = Location::whereType('island')->whereParentId($request->location_id)->get();
        }

        foreach ($series as $atoll) {

            $islandIds = Location::whereType('island')->whereParentId($atoll->id)->get()->pluck('id');

            if ($request->location_type == 'atoll') {
                $islandIds = [$atoll->id];
            }

            $feverCount = PeopleLog::whereIn('location_id', $islandIds)->where('fever', 1)->count();
            array_push($fever, ['name' => $atoll->name, 'value' => $feverCount]);

            $SobCount = PeopleLog::whereIn('location_id', $islandIds)->where('shortness_of_breath', 1)->count();
            array_push($sob, ['name' => $atoll->name, 'value' => $SobCount]);

            $coughCount = PeopleLog::whereIn('location_id', $islandIds)->where('cough', 1)->count();
            array_push($cough, ['name' => $atoll->name, 'value' => $coughCount]);

            $vulnerableCount = Vulnerable::whereIn('location_id', $islandIds)->count();
            array_push($vulnerable, ['name' => $atoll->name, 'value' => $vulnerableCount]);


        }

        return [
            ['name' => 'fever', 'series' => $fever],
            ['name' => 'sob', 'series' => $sob],
            ['name' => 'cough', 'series' => $cough],
            ['name' => 'vulnerable', 'series' => $vulnerable],
        ];
    }

    public function vulnerable(Request $request)
    {
        $data = [];
        if ($request->location_type == 'atolls') {
            $series = Location::whereType('atoll')->get();
        }

        if ($request->location_type == 'atoll' && $request->location_id) {
            $series = Location::whereType('island')->whereParentId($request->location_id)->get();
        }

        foreach ($series as $item) {

            $islandIds = Location::whereType('island')->whereParentId($item->id)->get()->pluck('id');

            if ($request->location_type == 'atoll') {
                $islandIds = [$item->id];
            }

            $count = Vulnerable::whereIn('location_id', $islandIds)->where($request->type, 1)->count();
            array_push($data, ['name' => $item->name, 'value' => $count]);
        }

        return ['name' => $request->type, 'series' => $data];
    }

    public function vulnerable_overview(Request $request){
        //hypertension
        //cardiovascular
        //chronic_respiratory_disease
        //cancer
        //old_age
        //pregnant
    }


}
