<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePeopleAddressAPIRequest;
use App\Http\Requests\API\UpdatePeopleAddressAPIRequest;
use App\Models\PeopleAddress;
use App\Repositories\PeopleAddressRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PeopleAddressController
 * @package App\Http\Controllers\API
 */

class PeopleAddressAPIController extends AppBaseController
{
    /** @var  PeopleAddressRepository */
    private $peopleAddressRepository;

    public function __construct(PeopleAddressRepository $peopleAddressRepo)
    {
        $this->peopleAddressRepository = $peopleAddressRepo;
    }

    /**
     * Display a listing of the PeopleAddress.
     * GET|HEAD /peopleAddresses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $peopleAddresses = $this->peopleAddressRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($peopleAddresses->toArray(), 'People Addresses retrieved successfully');
    }

    /**
     * Store a newly created PeopleAddress in storage.
     * POST /peopleAddresses
     *
     * @param CreatePeopleAddressAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePeopleAddressAPIRequest $request)
    {
        $input = $request->all();

        $peopleAddress = $this->peopleAddressRepository->create($input);

        return $this->sendResponse($peopleAddress->toArray(), 'People Address saved successfully');
    }

    /**
     * Display the specified PeopleAddress.
     * GET|HEAD /peopleAddresses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PeopleAddress $peopleAddress */
        $peopleAddress = $this->peopleAddressRepository->find($id);

        if (empty($peopleAddress)) {
            return $this->sendError('People Address not found');
        }

        return $this->sendResponse($peopleAddress->toArray(), 'People Address retrieved successfully');
    }

    /**
     * Update the specified PeopleAddress in storage.
     * PUT/PATCH /peopleAddresses/{id}
     *
     * @param int $id
     * @param UpdatePeopleAddressAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeopleAddressAPIRequest $request)
    {
        $input = $request->all();

        /** @var PeopleAddress $peopleAddress */
        $peopleAddress = $this->peopleAddressRepository->find($id);

        if (empty($peopleAddress)) {
            return $this->sendError('People Address not found');
        }

        $peopleAddress = $this->peopleAddressRepository->update($input, $id);

        return $this->sendResponse($peopleAddress->toArray(), 'PeopleAddress updated successfully');
    }

    /**
     * Remove the specified PeopleAddress from storage.
     * DELETE /peopleAddresses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PeopleAddress $peopleAddress */
        $peopleAddress = $this->peopleAddressRepository->find($id);

        if (empty($peopleAddress)) {
            return $this->sendError('People Address not found');
        }

        $peopleAddress->delete();

        return $this->sendSuccess('People Address deleted successfully');
    }
}
