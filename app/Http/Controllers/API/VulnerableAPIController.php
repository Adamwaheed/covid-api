<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVulnerableAPIRequest;
use App\Http\Requests\API\UpdateVulnerableAPIRequest;
use App\Models\Vulnerable;
use App\Repositories\VulnerableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class VulnerableController
 * @package App\Http\Controllers\API
 */
class VulnerableAPIController extends AppBaseController
{
    /** @var  VulnerableRepository */
    private $vulnerableRepository;

    public function __construct(VulnerableRepository $vulnerableRepo)
    {
        $this->vulnerableRepository = $vulnerableRepo;
    }

    /**
     * Display a listing of the Vulnerable.
     * GET|HEAD /vulnerables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $vulnerables = Vulnerable::with(['location', 'people', 'current_location']);


        if ($request->search && !is_null($request->search)) {

            $vulnerables->whereHas('people', function (Builder $query) use ($request) {

                if (is_numeric($request->search)) {

                    $query->where('contact', 'like', '%' . $request->search . '%');
                }

                if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $request->search)) {
                    dd(123);
                    $query->where('identifier', 'like', '%' . $request->search . '%');
                }

                if (preg_match('/^[a-zA-Z\s]+$/', $request->search)) {
                    $query->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('current_address', 'like', '%' . $request->search . '%');
                }


            });
        }


        if ($request->hypertension == 'true') {

            $vulnerables->where('hypertension', 1);

        }

        if ($request->cardiovascular == 'true') {

            $vulnerables->where('cardiovascular', 1);

        }


        if ($request->cancer == 'true') {

            $vulnerables->where('cancer', 1);

        }

        if ($request->old_age == 'true') {

            $vulnerables->where('old_age', 1);

        }

        if ($request->chronic_respiratory_disease == 'true') {

            $vulnerables->where('chronic_respiratory_disease', 1);

        }

        if ($request->pregnant == 'true') {

            $vulnerables->where('pregnant', 1);

        }


        return $this->sendResponse($vulnerables->paginate(), 'Vulnerables retrieved successfully');
    }

    /**
     * Store a newly created Vulnerable in storage.
     * POST /vulnerables
     *
     * @param CreateVulnerableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVulnerableAPIRequest $request)
    {
        $input = $request->all();

        $input['location_id'] = $request->current_location_id;

        $vulnerable = $this->vulnerableRepository->create($input);

        return $this->sendResponse($vulnerable->toArray(), 'Vulnerable saved successfully');
    }

    /**
     * Display the specified Vulnerable.
     * GET|HEAD /vulnerables/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Vulnerable $vulnerable */
        $vulnerable = $this->vulnerableRepository->find($id);

        if (empty($vulnerable)) {
            return $this->sendError('Vulnerable not found');
        }

        return $this->sendResponse($vulnerable->toArray(), 'Vulnerable retrieved successfully');
    }

    /**
     * Update the specified Vulnerable in storage.
     * PUT/PATCH /vulnerables/{id}
     *
     * @param int $id
     * @param UpdateVulnerableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVulnerableAPIRequest $request)
    {
        $input = $request->all();

        $input['location_id'] = $request->current_location_id;

        /** @var Vulnerable $vulnerable */
        $vulnerable = $this->vulnerableRepository->find($id);

        if (empty($vulnerable)) {
            return $this->sendError('Vulnerable not found');
        }

        $vulnerable = $this->vulnerableRepository->update($input, $id);

        return $this->sendResponse($vulnerable->toArray(), 'Vulnerable updated successfully');
    }

    /**
     * Remove the specified Vulnerable from storage.
     * DELETE /vulnerables/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var Vulnerable $vulnerable */
        $vulnerable = $this->vulnerableRepository->find($id);

        if (empty($vulnerable)) {
            return $this->sendError('Vulnerable not found');
        }

        $vulnerable->delete();

        return $this->sendSuccess('Vulnerable deleted successfully');
    }
}
