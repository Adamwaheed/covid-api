<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePeopleAPIRequest;
use App\Http\Requests\API\UpdatePeopleAPIRequest;
use App\Kanban;
use App\Models\Location;
use App\Models\People;
use App\Models\PeopleLocation;
use App\Models\PeopleLog;
use App\Repositories\PeopleRepository;
use App\Stats;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PeopleController
 * @package App\Http\Controllers\API
 */
class PeopleAPIController extends AppBaseController
{
    /** @var  PeopleRepository */
    private $peopleRepository;
    private $kanban;

    public function __construct(PeopleRepository $peopleRepo, Kanban $kanban)
    {
        $this->peopleRepository = $peopleRepo;
        $this->kanban = $kanban;
    }

    /**
     * Display a listing of the People.
     * GET|HEAD /people
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {


        $people = People::with(['location', 'vulnerable', 'lastLog']);

        if (is_numeric($request->search)) {

            $people->where('contact', 'like', '%' . $request->search . '%');
        }

        if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $request->search)) {

            $people->where('identifier', 'like', '%' . $request->search . '%');
        }

        if (preg_match('/^[a-zA-Z\s]+$/', $request->search)) {
            $people->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('current_address', 'like', '%' . $request->search . '%');
        }

        if ($request->status) {
            $people->where('status', $request->status);
        }


        return $this->sendResponse($people->paginate($request->pageSize), 'People retrieved successfully');
    }

    /**
     * Store a newly created People in storage.
     * POST /people
     *
     * @param CreatePeopleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePeopleAPIRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = auth()->user()->id;

        $input['positive'] = 0;

        $people = People::find($request->people_id)->with('lastLog')->first();
        if ($people->lastLog) {
            $input['location_id'] = $people->lastLog->location_id;
        } else {
            $input['location_id'] = $people->location_id;
        }



        $people = $this->peopleRepository->create($input);

        return $this->sendResponse($people->toArray(), 'People saved successfully');
    }

    /**
     * Display the specified People.
     * GET|HEAD /people/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var People $people */
        $people = People::with(['location', 'people_locations.location', 'people_locations.vessel', 'people_logs.location'])->find($id);

        if (empty($people)) {
            return $this->sendError('People not found');
        }

        return $this->sendResponse($people->toArray(), 'People retrieved successfully');
    }

    /**
     * Update the specified People in storage.
     * PUT/PATCH /people/{id}
     *
     * @param int $id
     * @param UpdatePeopleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeopleAPIRequest $request)
    {
        $input = $request->all();

        /** @var People $people */
        $people = $this->peopleRepository->find($id);

        if (empty($people)) {
            return $this->sendError('People not found');
        }

        $people = $this->peopleRepository->update($input, $id);

        return $this->sendResponse($people->toArray(), 'People updated successfully');
    }

    /**
     * Remove the specified People from storage.
     * DELETE /people/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var People $people */
        $people = $this->peopleRepository->find($id);

        if (empty($people)) {
            return $this->sendError('People not found');
        }

        $people->delete();

        return $this->sendSuccess('People deleted successfully');
    }

    public function dashboard()
    {
        $stats = new Stats(null, null, 'atoll', 3);
        return $stats->createSeries('fever');

//        $data['status'] = People::select(\DB::raw('count(*) as count'), 'status')
//            ->distinct()
//            ->groupBy('status')
//            ->get();

        $atoll = Location::whereType('atoll')->get();
        $feverArray = [];
        $shortness_of_breathArray = [];
        $coughArray = [];
        foreach ($atoll as $item) {

            $islandIds = Location::whereType('island')->whereParentId($item->id)->get()->pluck('id');

            $fever = PeopleLog::whereIn('location_id', $islandIds)->where('fever', 1)->count();

            array_push($feverArray, $fever);

            $shortness_of_breath = PeopleLog::whereIn('location_id', $islandIds)->where('shortness_of_breath', 1)->count();

            array_push($shortness_of_breathArray, $shortness_of_breath);

            $cough = PeopleLog::whereIn('location_id', $islandIds)->where('cough', 1)->count();

            array_push($coughArray, $cough);

        }


        return [$atoll, $feverArray, $shortness_of_breathArray, $coughArray];

    }

    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            return ['file' => 'file'];
        } else {
            'no fike';
        }
    }

    public function tests()
    {
        return $this->sendResponse($this->kanban->toString(), 'tests');
    }

    public function updateTest(Request $request)
    {
        $peopleLog = PeopleLog::find($request->people_log_id);
        if ($request->status == 'cured') {
            $peopleLog->test_result = 'positive';
            $peopleLog->status = 'normal';
        }

        if ($request->status == 'positive') {
            $peopleLog->test_result = 'positive';
            $peopleLog->status = 'hospitalized';
        }


        if ($request->status == 'negative') {
            $peopleLog->test_result = 'negative';
            $peopleLog->status = 'quarantine';
        }

    }

    public function homeIsolation(Request $request)
    {
        $people = People::whereHas('lastLog', function ($log) {
            $log->where('isolation_end_at', '>=', today());
        })->whereIn('status', ['home-isolation', 'quarantine'])->with('lastLog')->paginate();

        return $this->sendResponse($people->toArray(), 'People retrieved successfully');
    }

}
