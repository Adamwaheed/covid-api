<?php

namespace App\Http\Requests\API;

use App\Models\PeopleLog;
use InfyOm\Generator\Request\APIRequest;

/**
 * Class CreatePeopleLogAPIRequest
 * @package App\Http\Requests\API
 * @property string status
 * @property integer people_id
 */
class CreatePeopleLogAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'people_id'=>'required',
          'fever'=>'required',
          'shortness_of_breath'=>'required',
          'cough'=>'required',
          'status'=>'required',
        ];
    }
}
