<?php

namespace App\Http\Requests\API;

use App\Models\Vulnerable;
use InfyOm\Generator\Request\APIRequest;

/**
 * Class CreateVulnerableAPIRequest
 * @package App\Http\Requests\API
 * @property integer current_location_id;
 */
class CreateVulnerableAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Vulnerable::$rules;
    }
}
