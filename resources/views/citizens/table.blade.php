<div class="table-responsive">
    <table class="table" id="citizens-table">
        <thead>
            <tr>
                <th>Nationalid</th>
        <th>Name</th>
        <th>Age</th>
        <th>Contactnumber</th>
        <th>Gender</th>
        <th>Consit</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($citizens as $citizen)
            <tr>
                <td>{{ $citizen->NationalID }}</td>
            <td>{{ $citizen->name }}</td>
            <td>{{ $citizen->age }}</td>
            <td>{{ $citizen->contactNumber }}</td>
            <td>{{ $citizen->gender }}</td>
            <td>{{ $citizen->consit }}</td>
                <td>
                    {!! Form::open(['route' => ['citizens.destroy', $citizen->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('citizens.show', [$citizen->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('citizens.edit', [$citizen->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
