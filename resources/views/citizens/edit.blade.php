@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Citizen
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($citizen, ['route' => ['citizens.update', $citizen->id], 'method' => 'patch']) !!}

                        @include('citizens.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection