<!-- Nationalid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NationalID', 'Nationalid:') !!}
    {!! Form::text('NationalID', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Age Field -->
<div class="form-group col-sm-6">
    {!! Form::label('age', 'Age:') !!}
    {!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<!-- Contactnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contactNumber', 'Contactnumber:') !!}
    {!! Form::text('contactNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Consit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('consit', 'Consit:') !!}
    {!! Form::text('consit', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('citizens.index') }}" class="btn btn-default">Cancel</a>
</div>
