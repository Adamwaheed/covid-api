<!-- Nationalid Field -->
<div class="form-group">
    {!! Form::label('NationalID', 'Nationalid:') !!}
    <p>{{ $citizen->NationalID }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $citizen->name }}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Age:') !!}
    <p>{{ $citizen->age }}</p>
</div>

<!-- Contactnumber Field -->
<div class="form-group">
    {!! Form::label('contactNumber', 'Contactnumber:') !!}
    <p>{{ $citizen->contactNumber }}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{{ $citizen->gender }}</p>
</div>

<!-- Consit Field -->
<div class="form-group">
    {!! Form::label('consit', 'Consit:') !!}
    <p>{{ $citizen->consit }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $citizen->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $citizen->updated_at }}</p>
</div>

