<li class="{{ Request::is('locations*') ? 'active' : '' }}">
    <a href="{{ route('locations.index') }}"><i class="fa fa-edit"></i><span>Locations</span></a>
</li>

<li class="{{ Request::is('vessels*') ? 'active' : '' }}">
    <a href="{{ route('vessels.index') }}"><i class="fa fa-edit"></i><span>Vessels</span></a>
</li>

<li class="{{ Request::is('citizens*') ? 'active' : '' }}">
    <a href="{{ route('citizens.index') }}"><i class="fa fa-edit"></i><span>Citizens</span></a>
</li>

