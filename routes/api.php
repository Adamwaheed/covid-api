<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|peo
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'AuthAPIController@login');
Route::post('logout', 'AuthAPIController@logout');

Route::get('me', 'AuthAPIController@me');

Route::get('stats/over-view', 'StatsAPIController@overView');
Route::get('stats/dashboard', 'StatsAPIController@dashboard');
Route::get('stats/vulnerable', 'StatsAPIController@vulnerable');
Route::get('people/tests', 'PeopleAPIController@tests');
Route::get('people/home-isolation', 'PeopleAPIController@homeIsolation');

Route::get('locations/atolls', 'LocationAPIController@atolls');
Route::get('locations/residence', 'LocationAPIController@residence');
Route::get('locations/residenceStock', 'LocationAPIController@residenceStock');
Route::resource('locations', 'LocationAPIController');


Route::resource('users', 'UserAPIController');


Route::resource('people', 'PeopleAPIController');
Route::post('people/upload', 'PeopleAPIController@upload')->name('people.upload');

Route::resource('vessels', 'VesselAPIController');

Route::resource('people_locations', 'PeopleLocationAPIController');

Route::resource('people_logs', 'PeopleLogAPIController');

Route::resource('vulnerables', 'VulnerableAPIController');


Route::resource('people_addresses', 'PeopleAddressAPIController');

Route::resource('items', 'ItemAPIController');


Route::resource('location_items', 'LocationItemAPIController');


Route::resource('data_locks', 'DataLockAPIController');
